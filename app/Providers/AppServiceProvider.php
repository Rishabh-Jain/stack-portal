<?php

namespace App\Providers;

use App\Title;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    	$this->ViewShare();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function ViewShare()
	{
		$title = Title::grab()->first() ?? config('app.name');
		View::share('title',$title);
	}
}
