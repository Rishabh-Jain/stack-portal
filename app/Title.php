<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

class Title extends Model
{
    public function scopeGrab($query)
	{
		$slug = url()->current();
		return $query->where('slug',$slug)->select('title');
	}
}
